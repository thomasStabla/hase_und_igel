package sc.player2018.logic;

import java.lang.reflect.Array;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sc.player2018.Starter;
import sc.plugin2018.*;
import sc.plugin2018.util.Constants;
import sc.plugin2018.util.GameRuleLogic;
import sc.shared.PlayerColor;
import sc.shared.InvalidMoveException;
import sc.shared.GameResult;


public class StablaLogic implements IGameHandler {

	private Starter client;
	private GameState gameState;
	private Player currentPlayer;

	private int position;

	private int anzahlSalate;
	private int anzahlKarotten;

	private boolean karteSalatFressenVorhanden;
	private boolean karteKarrotenTauschenVorhanden;	
	private boolean karteFalleZurueckVorhanden;
	private boolean karteRueckeVorVorhanden;

	private int zugNr;


	private static final Logger log = LoggerFactory.getLogger(StablaLogic.class);

	private static final Random rand = new SecureRandom();


	public StablaLogic(Starter client) {
		this.client = client;
		position = 0;
		anzahlSalate = 5;
		anzahlKarotten = 68;

		karteSalatFressenVorhanden = true;
		karteKarrotenTauschenVorhanden = true;	
		karteFalleZurueckVorhanden = true;
		karteRueckeVorVorhanden = true;		

		zugNr = 0;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onRequestAction(){	
		zugNr++;

		logStabla("Es wurde ein Zug angefordert: Nr. " + zugNr);


		long startTime = System.nanoTime();

		ArrayList<Move> possibleMoves = gameState.getPossibleMoves(); // Enthält mindestens ein Element

		for (Move move : possibleMoves) {
			logStabla(move.toString());

			for (Action action : move.actions) {
				logStabla(action.toString());
			}
		}

		Move myMove = findShortestAdvance(possibleMoves);
		if(myMove == null) {
			myMove = possibleMoves.get(0);
		}
			

		long nowTime = System.nanoTime();
		sendAction(myMove);
		logStabla("Es wurde ein Zug gesendet");
		logStabla(myMove.toString());

	}

	private Move findShortestAdvance(ArrayList<Move> possibleMoves) {
		for (Move move : possibleMoves) {
			for (Action action : move.actions) {
				if(action instanceof Advance) 
					return move;
			}
		}
		return null;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onUpdate(Player player, Player otherPlayer) {
		currentPlayer = player;
		log.info("Spielerwechsel: " + player.getPlayerColor());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onUpdate(GameState gameState) {
		this.gameState = gameState;
		currentPlayer = gameState.getCurrentPlayer();
		log.info("Das Spiel geht voran: Zug: {}", gameState.getTurn());
		log.info("Spieler: {}", currentPlayer.getPlayerColor());
	}

	/**
	 * {@inheritDoc}
	 */
	public void gameEnded(GameResult data, PlayerColor color,
			String errorMessage) {
		log.info("Das Spiel ist beendet.");
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void sendAction(Move move) {
		client.sendMove(move);
	}

	public void logStabla(String message) {
		System.out.println("StablaLogic:" + message);

	}
}
